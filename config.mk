PRODUCT_SOONG_NAMESPACES += \
    packages/apps/memecam

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/bin,$(TARGET_COPY_OUT_SYSTEM)/bin) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/priv-app/ANXCamera/lib/arm,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib/arm) \
    $(call find-copy-subdir-files,*,packages/apps/memecam/system/priv-app/ANXCamera/lib/arm64,$(TARGET_COPY_OUT_SYSTEM)/priv-app/ANXCamera/lib/arm64)

PRODUCT_PACKAGES += \
    ANXCamera

PRODUCT_PRODUCT_PROPERTIES += \
    ro.miui.notch=1

# Build
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true
